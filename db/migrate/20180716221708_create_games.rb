class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.integer "rounds", default: 0
      t.integer "playerwins", default: 0
      t.integer "computerwins", default: 0

      t.timestamps
    end
  end
end
